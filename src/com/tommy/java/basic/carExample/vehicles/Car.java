package com.tommy.java.basic.carExample.vehicles;


import com.tommy.java.basic.carExample.details.Engine;
import com.tommy.java.basic.carExample.professions.Driver;

public class Car {
    private String producer;
    private String aClass;
    private double weight;
    private Driver driver;
    private Engine engine;
    private boolean headlightsOn;
    private boolean turnSignalsOn;
    private boolean backlightsOn;
    public Car() {
    }

    public String getProducer() {
        return producer;
    }
    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getaClass() {
        return aClass;
    }
    public void setaClass(String aClass) {
        this.aClass = aClass;
    }

    public double getWeight() {
        return weight;
    }
    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Driver getDriver() {
        return driver;
    }
    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Engine getEngine() {
        return engine;
    }
    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public void start() {
        System.out.println("Поїхали");
    }
    public void stop() {
        System.out.println("Зупиняємося");
    }
    public void turnRight() {
        System.out.println("Поворот направо");
    }
    public void turnLeft() {
        System.out.println("Поворот наліво");
    }
    public void openTrunk(){
        System.out.println("Відкриваємо багажник");
    }
    public void closeTrunk(){
        System.out.println("Закриваємо багажник");
    }
    public void switchHeadlights(){
        this.headlightsOn = !this.headlightsOn;
        System.out.println("Фари " + (this.headlightsOn ? "ввімкнені" : "вимкнені"));
    }
    public void switchTurnSignals(){
        this.turnSignalsOn = !this.turnSignalsOn;
        System.out.println("Поворот " + (this.turnSignalsOn ? "ввімкнено" : "вимкнено"));
    }
    public void switchBacklights(){
        this.backlightsOn = !this.backlightsOn;
        System.out.println("Задні фари " + (this.backlightsOn ? "ввімкнені" : "вимкнені"));
    }
    public void switchDrivingMode(String mode) {
        System.out.println("Режим водіння машини змінено на '" + mode + "'");
    }

    @Override
    public String toString() {
        return "Car{" +
                "producer='" + producer + '\'' +
                ", aClass='" + aClass + '\'' +
                ", weight=" + weight +
                ", driver=" + driver +
                ", engine=" + engine +
                ", headlightsOn=" + headlightsOn +
                ", turnSignalsOn=" + turnSignalsOn +
                ", backlightsOn=" + backlightsOn +
                '}';
    }
}
