package com.tommy.java.basic.carExample;

import com.tommy.java.basic.carExample.details.Engine;
import com.tommy.java.basic.carExample.professions.Driver;
import com.tommy.java.basic.carExample.vehicles.*;

public class CarDemo {
    public static void main(String[] args) {
        // create a Car object
        Car car = new Car();
        car.setProducer("Toyota");
        car.setaClass("SUV");
        car.setWeight(1800);
        // create a Driver object
        Driver carDriver = new Driver("John Smith", 25, 3);
        // create an Engine object
        Engine carEngine = new Engine("200 hp", "Toyota");
        // set Driver and Engine for Car
        car.setDriver(carDriver);
        car.setEngine(carEngine);
        // invoke methods
        car.start();
        car.openTrunk();
        car.closeTrunk();
        car.switchHeadlights();
        car.switchDrivingMode("Місто");
        car.stop();
        // create a Lorry object
        Lorry lorry = new Lorry();
        lorry.setProducer("Man");
        lorry.setaClass("Heavy Duty Truck");
        lorry.setWeight(15000);
        lorry.setLiftingCapacity(20000);
        // create a Driver object
        Driver lorryDriver = new Driver("Jane Doe", 35, 10);
        // create an Engine object
        Engine lorryEngine = new Engine("500 hp", "Man");
        // set Driver and Engine for Lorry
        lorry.setDriver(lorryDriver);
        lorry.setEngine(lorryEngine);
        // invoke methods
        lorry.start();
        lorry.openTrunk();
        lorry.closeTrunk();
        lorry.switchBacklights();
        lorry.switchDrivingMode("Місто");
        lorry.turnLeft();
        lorry.stop();
        // create a SportCar object
        SportCar sportCar = new SportCar();
        sportCar.setProducer("Ferrari");
        sportCar.setaClass("Sports");
        sportCar.setWeight(1200);
        sportCar.setSpeed(300);
        // create a Driver object
        Driver sportCarDriver = new Driver("John Doe", 29, 5);
        // create an Engine object
        Engine sportCarEngine = new Engine("800 hp", "Ferrari");
        // set Driver and Engine for SportCar
        sportCar.setDriver(sportCarDriver);
        sportCar.setEngine(sportCarEngine);
        // invoke methods
        sportCar.start();
        sportCar.openTrunk();
        sportCar.closeTrunk();
        sportCar.switchTurnSignals();
        sportCar.switchDrivingMode("Спорт");
        sportCar.turnRight();
        sportCar.stop();

        System.out.println(car);
        System.out.println(lorry);
        System.out.println(sportCar);
    }
}